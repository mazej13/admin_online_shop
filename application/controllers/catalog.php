<?php

  class Application_Controllers_Catalog extends Lib_BaseController
  {
     function __construct()
	 {	
		 
	
			$page=1;//показать первую страницу выбранного раздела
			$step=6;//сколько выводить на странице объектов	
			$product_sub_category=1;	
			
			if(isset($_REQUEST['p'])){ 
				$page=$_REQUEST['p'];
			}
		
			$model=new Application_Models_Catalog; 
			
			
			$model->category_id=Lib_Category::getInstance()->getCategoryList($_REQUEST['category_id']);
			$model->category_id[]=$_REQUEST['category_id'];// в конец списка, добавляем корневую текущую категорию
			
			$Items =$model->getPageList($page,$step);
			$this->pager=$Items['pagination'];
			unset($Items['pagination']);

			$this->TiteCategory=$model->current_category["title"];//наименование текущей категории
			$this->Items=$Items;//список продуктов выводимых в этой категории
		
	}
  }